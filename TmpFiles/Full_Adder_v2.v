//module fa(s,co,a,b,ci);
//    output s,co;
//    input a,b,ci;
//    xor1 u1(s,a,b,ci);
//    and1 u2(n1,a,b);
//    and1 u3(n2,b,ci);
//    and1 u4(n3,a,ci);
//    or1 u5(co,n1,n2,n3);
//endmodule

module full_adderall 
  (
   i_bit1,
   i_bit2,
   i_carry,
   o_sum,
   o_carry
   );
 
  input  i_bit1;
  input  i_bit2;
  input  i_carry;
  output o_sum;
  output o_carry;
 
//  wire   w_WIRE_1;
//  wire   w_WIRE_2;
//  wire   w_WIRE_3;
//       
//  assign w_WIRE_1 = i_bit1 ^ i_bit2;
//  assign w_WIRE_2 = w_WIRE_1 & i_carry;
//  assign w_WIRE_3 = i_bit1 & i_bit2;
// 
//  assign o_sum   = w_WIRE_1 ^ i_carry;
//  assign o_carry = w_WIRE_2 | w_WIRE_3;
 
 
  // FYI: Code above using wires will produce the same results as:
  assign o_sum   = i_bit1 ^ i_bit2 ^ i_carry;
  assign o_carry = (i_bit1 ^ i_bit2) & i_carry) | (i_bit1 & i_bit2);
   
endmodule 