//module full_adder(bit1,bit2,cin,sum,car);
//	input bit1,bit2,cin;
//	output sum,car;
//	
//	wire w1,w2,w3;
//	xor w1g1(w1,bit1,bit2);
//	and w2g1(w2,bit1,bit2);
//	and w3g1(w3,w1,cin);
//	or w2w3g1(cout,w2,w3);
//	xor outg2(sum,w1,cin);
	
//	xor sumBit(bit1,bit2,cin);
//	and carryPt1(bit1,bit2);
//	xor carryPt2(bit1,bit2);
//	and carryPt3(cin,carryPt2);
//	or carryBit(carryPt1,carryPt3);
//endmodule

//module full_adder 
//  (
//   i_bit1,
//   i_bit2,
//   i_carry,
//   o_sum,
//   o_carry
//   );
// 
//  input  i_bit1;
//  input  i_bit2;
//  input  i_carry;
//  output o_sum;
//  output o_carry;
// 
//  wire   w_WIRE_1;
//  wire   w_WIRE_2;
//  wire   w_WIRE_3;
//       
//  assign w_WIRE_1 = i_bit1 ^ i_bit2;
//  assign w_WIRE_2 = w_WIRE_1 & i_carry;
//  assign w_WIRE_3 = i_bit1 & i_bit2;
// 
//  assign o_sum   = w_WIRE_1 ^ i_carry;
//  assign o_carry = w_WIRE_2 | w_WIRE_3;
//endmodule

module fa(s,co,a,b,ci);
    output s,co;
    input a,b,ci;
    xor1 u1(s,a,b,ci);
    and1 u2(n1,a,b);
    and1 u3(n2,b,ci);
    and1 u4(n3,a,ci);
    or1 u5(co,n1,n2,n3);
endmodule