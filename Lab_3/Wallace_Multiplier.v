// Borrowed from FPGA
module five_bit_adder(input1,input2,answer,carry_out);
parameter N=5;
input [N-1:0] input1,input2;
   output [N-1:0] answer;
  wire [N-1:0] carry;
   genvar i;
   generate 
   for(i=0;i<N;i=i+1)
     begin: generate_5_bit_Adder
   if(i==0) 
  half_adder f(input1[0],input2[0],answer[0],carry[0]);
   else
  full_adder f(input1[i],input2[i],carry[i-1],answer[i],carry[i]);
     end
  assign carry_out = carry[N-1];
   endgenerate
endmodule 

// fpga4student.com: FPGA projects, Verilog projects, VHDL projects
// Verilog project: Verilog code for N-bit Adder 
// Verilog code for half adder 
module half_adder(x,y,s,c);
   input x,y;
   output s,c;
   assign s=x^y;
   assign c=x&y;
endmodule // half adder

// fpga4student.com: FPGA projects, Verilog projects, VHDL projects
// Verilog project: Verilog code for N-bit Adder 
// Verilog code for full adder 
module full_adder(x,y,c_in,s,c_out);
   input x,y,c_in;
   output s,c_out;
 assign s = (x^y) ^ c_in;
 assign c_out = (y&c_in)| (x&y) | (x&c_in);
endmodule // full_adder


module wallace_five_by_five(input a[4:0], input b[4:0], output c_out[4:0], output final_sum[4:0])
	half_adder HA1(
		.x(a[0]&b[1]),
		.y(a[1]&b[0]),
		.s(final_sum[0]),
		.c_out(c_out[0])
	)


	full_adder FA1(
		.x(a[0]&b[2]),
		.y(a[1]&b[1]),
		.c_in(a[2]&b[0]),
		.s(final_sum[1]),
		.c_out(c_out[1])
	)
	
	full_adder FA2(
		.x(a[0]&b[3]),
		.y(a[1]&b[2]),
		.c_in(a[2]&b[1]),
		.s(final_sum[2]),
		.c_out(c_out[2])
	)
	
	full_adder FA3(
		.x(a[0]&b[4]),
		.y(a[1]&b[3]),
		.c_in(a[3]&b[1]),
		.s(final_sum[3]),
		.c_out(c_out[3])
	)
	
	full_adder FA4(
		.x(a[3]&b[2]),
		.y(a[2]&b[3]),
		.c_in(1'b0),
		.s(final_sum[4]),
		.c_out(c_out[4])
	)
	
endmodule

module wallace_five_by_five_s2(input a[4:0], input b[4:0], input c[4:0], output c_out[4:0], output final_sum[4:0])
	half_adder HA1(
		.x(a[0]),
		.y(b[0]),
		.s(final_sum[0]),
		.c_out(c_out[0])
	)


	full_adder FA1(
		.x(a[1]),
		.y(b[1]),
		.c_in(c[0]),
		.s(final_sum[1]),
		.c_out(c_out[1])
	)
	
	full_adder FA2(
		.x(a[2]),
		.y(b[2]),
		.c_in(c[1]),
		.s(final_sum[2]),
		.c_out(c_out[2])
	)
	
	full_adder FA3(
		.x(a[3]),
		.y(b[3]),
		.c_in(c[2]),
		.s(final_sum[3]),
		.c_out(c_out[3])
	)
	
	full_adder FA4(
		.x(a[4]),
		.y(b[4]),
		.c_in(c[3]),
		.s(final_sum[4]),
		.c_out(c_out[4])
	)
	
	
	
endmodule 
module five_five_final(input a[4:0], input b[4:0], output fun[9:0])
	wire outputs1[4:0];
	wire cout1[4:0];
	wire as2[4:0];
	wire bs2[4:0];
	wire cins2[3:0];
	wire outputs2[4:0];
	wire cout2[4:0];
	wire adda[4:0];
	wire addb[4:0];
	wire addout[4:0];
	wire addcarry;
	
	assign wallace_s1 wallace_five_by_five(
		.a(a),
		.b(b),
		.c_out(cout1),
		.final_sum(outputs1)
		)

		as2=[cout1[4],cout1[3],cout1[2],cout1[1],cout1[0]];
		bs2=[a[3]&b[4],outputs1[4],outputs1[3],outputs1[2],outputs1[1]];
		cins2=[a[4]b[3],a[1]&b[4],a[2]&b[2],a[3]&b[0]];

	assign wallace_s2 wallace_five_by_five_s2(
		.a(as2), 
		.b(bs2), 
		.c(cins2), .
		.c_out(cout2),
		.final_sum(outputs2)
	)
	
	assign adda=[cout2[4],cout2[3],cout2[2],cout2[1],cout2[0]];
	assign addb=[a[4]&b[4],outputs2[4],outputs2[3],outputs2[2],outputs2[1]];
	
	
	assign five_bit_adder FBA(
	.input1(add3),
	.input2(addb),
	.answer(addout),
	.carry_out(addout)
	)
	
	//vector merge
	assign fun[0] = a[0]&b[0];
	assign fun[1] = outputs1[0];
	assign fun[2] = outputs2[0];
	assign fun[8:3] = addout;
	assign fun[9] = addout;
	

endmodule
