module half_adder(x,y,s,c);
   input x,y;
   output s,c;
   assign s=x^y;
   assign c=x&y;
endmodule


module full_adder(x,y,c_in,s,c_out);
   input x,y,c_in;
   output s,c_out;
 assign s = (x^y) ^ c_in;
 assign c_out = (y&c_in)| (x&y) | (x&c_in);
endmodule


module five_by_five_wallace_tree(input A[4:0], input B[4:0], output product[9:0]);
    //arrays above need to be PACKED: input [xx:yy] NAME for the vectorized operation to work.
	 
	 //B_i dotted with A
    assign  init ={B[0]&A[0],B[0]&A[1],B[0]&A[2],B[0]&A[3],B[0]&A[4]};
    assign  r1 =  {B[1]&A[0],B[1]&A[1],B[1]&A[2],B[1]&A[3],B[1]&A[4]};
    assign  r2 =  {B[2]&A[0],B[2]&A[1],B[2]&A[2],B[2]&A[3],B[2]&A[4]};
    assign  r3 =  {B[3]&A[0],B[3]&A[1],B[3]&A[2],B[3]&A[3],B[3]&A[4]};
	 assign  r4 =  {B[4]&A[0],B[4]&A[1],B[4]&A[2],B[4]&A[3],B[4]&A[4]};
	 
	 //freebie
	 assign product[0] = init[0];

	 //align the rows - 4 bit adder with a carry would be slightly more optimal.
	 assign r0 = {1'b0,init[4:1]};
	 wire [5:0] cout1,sum1; //can reinstantiate as singleton wires between adders.
	 
	 //first two rows
    half_adder ha1(r0[0],r1[0],sum1[0],cout1[0]);
    full_adder fa1(r0[1],r1[1],cout1[0],sum1[1],cout1[1]);
	 full_adder fa2(r0[2],r1[2],cout1[1],sum1[2],cout1[2]);
	 full_adder fa3(r0[3],r1[3],cout1[2],sum1[3],cout1[3]);
	 full_adder fa4(r0[4],r1[4],cout1[3],sum1[4],cout1[4]);
	 
	 //write the lowest weighted bit
	 assign product[1] = sum1[0];
	 
	 
	 //running sum and third row, r2
	 assign rsum1 = {cout1[4],sum1[4:1]};
	 wire [5:0] cout2,sum2;
	 half_adder ha2(rsum1[0],r2[0],sum2[0],cout2[0]);
    full_adder fa5(rsum1[1],r2[1],cout2[0],sum2[1],cout2[1]);
	 full_adder fa6(rsum1[2],r2[2],cout2[1],sum2[2],cout2[2]);
	 full_adder fa7(rsum1[3],r2[3],cout2[2],sum2[3],cout2[3]);
	 full_adder fa8(rsum1[4],r2[4],cout2[3],sum2[4],cout2[4]);
	 
	 //write the lowest weighted bit
	 assign product[2] = sum2[0];

	 
	 //running sum and fourth row, r3
	 assign rsum2 = {cout2[4],sum2[4:1]};
	 assign r4 = p4;
	 wire [5:0] cout3,sum3;
	 half_adder ha3(rsum2[0],r3[0],sum3[0],cout3[0]);
    full_adder fa9(rsum2[1],r3[1],cout3[0],sum3[1],cout3[1]);
	 full_adder fa10(rsum2[2],r3[2],cout3[1],sum3[2],cout3[2]);
	 full_adder fa11(rsum2[3],r3[3],cout3[2],sum3[3],cout3[3]);
	 full_adder fa12(rsum2[4],r3[4],cout3[3],sum3[4],cout3[4]);
	 
	 //write the lowest weighted bit
	 assign product[3] = sum3[0];
	 
	 //running sum and fifth row, r4
	 assign rsum3 = {cout3[4],sum3[4:1]};
	 wire [5:0] cout4,sum4;
	 half_adder ha4(rsum3[0],r4[0],sum4[0],cout4[0]);
    full_adder fa13(rsum3[1],r4[1],cout4[0],sum4[1],cout4[1]);
	 full_adder fa14(rsum3[2],r4[2],cout4[1],sum4[2],cout4[2]);
	 full_adder fa15(rsum3[3],r4[3],cout4[2],sum4[3],cout4[3]);
	 full_adder fa16(rsum3[4],r4[4],cout4[3],sum4[4],cout4[4]);
	 
	 //last row, write to final product
	 assign product[9:4] = {cout[4],sum4};


endmodule
