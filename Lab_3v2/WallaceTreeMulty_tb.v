//copied from http://verilogcodes.blogspot.com/2015/11/verilog-code-for-4-bit-wallace-tree.html

module five_by_five_wallace_tree_tb;

    // Inputs
    reg [4:0] A;
    reg [4:0] B;

    // Outputs
    wire [9:0] prod;
    integer i,j,error;

    // Instantiate the Unit Under Test (UUT)
    wallace uut (
        .A(A), 
        .B(B), 
        .prod(prod)
    );

    initial begin
        // Apply inputs for the whole range of A and B.
        // 32*32 = 1024 inputs.
        error = 0;
        for(i=0;i <=32;i = i+1)
            for(j=0;j <=32;j = j+1) 
            begin
                A <= i; 
                B <= j;
                #1;
                if(prod != A*B) //if the result isnt correct increment "error".
                    error = error + 1;  
            end     
    end
      
endmodule
