//testbench for final_adder.v
`timescale 1 ns / 100 ps
module ripple_carry_add_testbench();
	reg [3:0] A;
	reg [3:0] B;
	reg sin; //implement a switch for negative? current default is 0
	
	wire [3:0] SUM;
	wire cout;
	
	ripple_carry_adder unit_test (
		.i_num1(A),
		.i_num2(B),
		.i_sign(sin),
		.o_res(SUM),
		.o_overflow(cout)
	);
	
	//adopted from tjeyamy.blogspot.com
	
	initial begin
		A = 0;
		B = 0;
		sin = 0;
		#100 //voodoo wait time?
		
		A=4'b0001;B=4'b0000;sin=1'b0;
		#10
		A=4'b1010;B=4'b0011;sin=1'b0;
		#10
		A=4'b1101;B=4'b1010;sin=1'b0;
		#10
		A=4'b0000;B=4'b0000;sin=1'b0;
	end 
	
	initial begin //just the print statement?
		$monitor("time=",$time,,"A=%b B=%b sin=%b : SUM=%b cout=%b",A,B,sin,SUM,cout);
	end 
	// end test cases
endmodule 
		
		
		