module final_full_adder(input i_b1,input i_b2,input i_cin,output o_cout,output o_sum);
	
	wire w1;
	wire w2;
	wire w3;
	
	assign w1 = i_b1 ^ i_b2;
	assign w2 = w1 & i_cin;
	assign w3 = i_b1 & i_b2;
	
	assign o_sum   = w1 ^ i_cin;
	assign o_cout = w3 | w2;
endmodule 

module ripple_carry_adder(input [3:0] i_num1,input [3:0]i_num2,input i_sign,output [3:0] o_res,output o_overflow);
	wire [4:0]    w_CARRY;
	wire [3:0]    w_SUM;
   
//	assign w_CARRY[0] = 1'b0; //implement a flip if it's negative
	assign w_CARRY[0] = i_sign;
   
	final_full_adder full_adder_1
		( 
		.i_b1(i_num1[0]),
		.i_b2(i_num2[0]),
		.i_cin(w_CARRY[0]),
		.o_sum(w_SUM[0]),
		.o_cout(w_CARRY[1])
		);
	
	final_full_adder full_adder_2
		( 
		.i_b1(i_num1[1]),
		.i_b2(i_num2[1]),
		.i_cin(w_CARRY[1]),
		.o_sum(w_SUM[1]),
		.o_cout(w_CARRY[2])
		);

		final_full_adder full_adder_3
		( 
		.i_b1(i_num1[2]),
		.i_b2(i_num2[2]),
		.i_cin(w_CARRY[2]),
		.o_sum(w_SUM[2]),
		.o_cout(w_CARRY[3])
		);

		final_full_adder full_adder_4
		( 
		.i_b1(i_num1[3]),
		.i_b2(i_num2[3]),
		.i_cin(w_CARRY[3]),
		.o_sum(w_SUM[3]),
		.o_cout(w_CARRY[4])
		);

		assign o_overflow = w_CARRY[4]; // ^ w_CARRY[3];
		//assign o_res = {w_CARRY[4], w_SUM}; //drop on last dig?
		assign o_res = w_SUM;
 
endmodule 