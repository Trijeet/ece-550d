//set timescale
`timescale 1ns / 100ps
module nand_gate_tb();

//declarations
reg a;
reg b;

reg clock;
wire out;
	
integer num_errors;

nand_gate test_nand_gate(a,b,out);

//simulation
initial begin
	$display($time, " simulation start");
	
	clock = 1'b0;
	num_errors = 0;
	
	@(negedge clock); //when clock goes negative?
	a = 1'b0;
	b = 1'b0;
	
	@(negedge clock);
	if (out != 1'b1) begin
		$display("a: %b b: %b Error: expected 1 but got %b", a,b,out);
		num_errors = num_errors + 1;
	end
	
	@(negedge clock);
	a = 1'b0;
	b = 1'b1;
	
	@(negedge clock);
	a = 1'b1;
	b = 1'b0;
	
	@(negedge clock);
	if (out != 1'b1) begin
		$display("a: %b b: %b Error: expected 0 but got %b", a,b,out);
		num_errors = num_errors + 1;
	end
	
	@(negedge clock);
	a = 1'b1;
	b = 1'b1;
	
	@(negedge clock);
	if (out != 1'b1) begin
		$display("a: %b b: %b Error: expected 0 but got %b", a,b,out);
		num_errors = num_errors + 1;
	end
	
	if (num_errors == 0) begin
		$display("Simulation succeeded with no errors.");
	end else begin
		$display("Simulation failed with %b error(s).", num_errors);
		end

end

always
	#10 clock = ~clock; //this toggles the clock every 10 timescale units (set at beginning)
	
endmodule
	