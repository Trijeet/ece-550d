module dec2to4(out, In);
	//In and Out
	input [1:0] In;
	output [4:0] out;
	
	//not of the in (n)
	wire [1:0] nIn;
	
	//not gates to in 
	not not0(nIn[0], In[0]);
	not not1(nIn[1], In[1]);
	
	//decode happens with AND
	and and0(out[0], nIn[1], nIn[0]);
	and and1(out[1], nIn[1], In[0]);
	and and2(out[2], In[1], nIn[0]);
	and and3(out[3], In[1], In[0]);

endmodule
